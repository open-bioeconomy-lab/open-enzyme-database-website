# Open Enzyme Database Website

The project is to build a website to have the database for various enzymes available at one place, well organized and easily accessible for everyone.

# Design of the Website

-> A table showing the entire collection for the user to choose from. Further, search filters based on different categories for convenience.

-> A separate page for each enzyme having all it's properties.
